/**
 * Core
 */
window.$ = window.jQuery = require('jquery');
window._ = require('lodash');
window.Vue = require('vue');
window.axios = require('axios');
window.GLightbox = require('glightbox');
window.flatpickr = require('flatpickr');
require('bootstrap-sass');
require('owl.carousel');
window.Swal = require('sweetalert2');