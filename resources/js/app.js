require('./bootstrap');

Vue.component('book', require('./components/book.vue').default);
Vue.component('contact', require('./components/contact.vue').default);
Vue.component('mobile-menu', require('./components/mobile-menu.vue').default);

window.VueInstance = new Vue().$mount('#app-vue');