<footer>
    <div class="container">
        <div class="row row-xl">
            <div class="col-md-2">
                <img src="/images/logo-cutout-white-full.png" class="img-responsive" />
            </div>
            <div class="col-md-8">
                @include('components.navigation')
            </div>
            <div class="col-md-2">
                <ul class="socials">
                    <li><a href="https://www.facebook.com/geordiecrawl01" target="_blank"><span class="fab fa-facebook-square"></span></a></li>
                    <li><a href="https://www.instagram.com/geordiecrawl" target="_blank"><span class="fab fa-instagram-square"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<footer class="post">
    <div class="container">
        <p>Copyright &copy; 2022 Geordie Crawl<span> | All Rights Reserved <a href="#" class="pull-right">Website by <strong>Letushost</strong></a></span></p>
    </div>
</footer>