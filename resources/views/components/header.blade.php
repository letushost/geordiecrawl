<header class="main">
	<div class="container">
        <div class="row row-condensed">
            <div class="col-md-2 col-xs-2">
                <img src="/images/logo-black-new.png" class="logo" />
            </div>
            <div class="col-md-8 hidden-sm hidden-xs">
                @include('components.navigation')
            </div>
            <div class="col-md-2 col-xs-12">
                <a href="{{ route('book') }}" class="btn btn-primary">Book now</a>
            </div>
        </div>
    </div>

    <a href="#" class="open-menu" @click.prevent="$root.$emit('toggleSidebar')"><span class="fa fa-bars"></span></a>
</header>