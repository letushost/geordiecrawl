<section class="book-banner">
    <div class="container">
        <div class="row row-lg">
            <div class="col-md-8">
                <h2>Ready for your <span>belta</span> of a <em>pub crawl</em>?</h2>
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{ route('book') }}" class="btn btn-primary">Book me up</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>