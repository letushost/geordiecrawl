<ul>
    <li><a href="{{ route('home') }}" @if(Route::currentRouteName() == 'home') class="active" @endif>Home</a></li>
    <li><a href="{{ route('gallery') }}" @if(Route::currentRouteName() == 'gallery') class="active" @endif>Gallery</a></li>
    <li><a href="{{ route('faqs') }}" @if(Route::currentRouteName() == 'faqs') class="active" @endif>FAQs</a></li>
    <li><a href="{{ route('contact') }}" @if(Route::currentRouteName() == 'contact') class="active" @endif>Contact</a></li>
</ul>