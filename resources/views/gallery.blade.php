@extends('layout.main', ['header' => true])

@section('content')
<div class="page page-gallery">
	@include('components.book-banner')
	
	<div class="grid-wrapper">
		<a href="/images/gallery/1.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/1.jpg" alt="" />
		</a>
		<a href="/images/gallery/2.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/2.jpg" alt="" />
		</a>
		<a href="/images/gallery/3.jpg" class="tall glightbox" data-gallery="gallery">
			<img src="/images/gallery/3.jpg" alt="">
		</a>
		<a href="/images/gallery/4.jpg" class="wide glightbox" data-gallery="gallery">
			<img src="/images/gallery/4.jpg" alt="" />
		</a>
		<a href="/images/gallery/5.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/5.jpg" alt="" />
		</a>
		<a href="/images/gallery/6.jpg" class="tall glightbox" data-gallery="gallery">
			<img src="/images/gallery/6.jpg" alt="" />
		</a>
		<a href="/images/gallery/7.jpg" class="big glightbox" data-gallery="gallery">
			<img src="/images/gallery/7.jpg" alt="" />
		</a>
		<a href="/images/gallery/8.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/8.jpg" alt="" />
		</a>
		<a href="/images/gallery/9.jpg" class="wide glightbox" data-gallery="gallery">
			<img src="/images/gallery/9.jpg" alt="" />
		</a>
		<a href="/images/gallery/10.jpg" class="big glightbox" data-gallery="gallery">
			<img src="/images/gallery/10.jpg" alt="" />
		</a>
		<a href="/images/gallery/11.jpg" class="tall glightbox" data-gallery="gallery">
			<img src="/images/gallery/11.jpg" alt="" />
		</a>
		<a href="/images/gallery/12.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/12.jpg" alt="" />
		</a>
		<a href="/images/gallery/13.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/13.jpg" alt="" />
		</a>
		<a href="/images/gallery/14.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/14.jpg" alt="" />
		</a>
		<a href="/images/gallery/15.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/15.jpg" alt="" />
		</a>
		<a href="/images/gallery/16.jpg" class="wide glightbox" data-gallery="gallery">
			<img src="/images/gallery/16.jpg" alt="" />
		</a>
		<a href="/images/gallery/17.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/17.jpg" alt="" />
		</a>
		<a href="/images/gallery/18.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/18.jpg" alt="" />
		</a>
		<a href="/images/gallery/19.jpg" class="wide glightbox" data-gallery="gallery">
			<img src="/images/gallery/19.jpg" alt="" />
		</a>
		<a href="/images/gallery/20.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/20.jpg" alt="" />
		</a>
		<a href="/images/gallery/21.jpg" class="wide glightbox" data-gallery="gallery">
			<img src="/images/gallery/21.jpg" alt="" />
		</a>
		<a href="/images/gallery/22.jpg" class="big glightbox" data-gallery="gallery">
			<img src="/images/gallery/22.jpg" alt="" />
		</a>
		<a href="/images/gallery/23.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/23.jpg" alt="" />
		</a>
		<a href="/images/gallery/24.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/24.jpg" alt="" />
		</a>
		<a href="/images/gallery/25.jpg" class="big glightbox" data-gallery="gallery">
			<img src="/images/gallery/25.jpg" alt="" />
		</a>
		<a href="/images/gallery/26.jpg" class="tall glightbox" data-gallery="gallery">
			<img src="/images/gallery/26.jpg" alt="" />
		</a>
		<a href="/images/gallery/27.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/27.jpg" alt="" />
		</a>
		<a href="/images/gallery/28.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/28.jpg" alt="" />
		</a>
		<a href="/images/gallery/29.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/29.jpg" alt="" />
		</a>
		<a href="/images/gallery/30.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/30.jpg" alt="" />
		</a>
		<a href="/images/gallery/31.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/31.jpg" alt="" />
		</a>
		<a href="/images/gallery/32.jpg" class="tall glightbox" data-gallery="gallery">
			<img src="/images/gallery/32.jpg" alt="">
		</a>
		<a href="/images/gallery/33.jpg" class="wide glightbox" data-gallery="gallery">
			<img src="/images/gallery/33.jpg" alt="" style="object-position: top;" />
		</a>
		<a href="/images/gallery/34.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/34.jpg" alt="" />
		</a>
		<a href="/images/gallery/35.jpg" class="tall glightbox" data-gallery="gallery">
			<img src="/images/gallery/35.jpg" alt="" />
		</a>
		<a href="/images/gallery/36.jpg" class="big glightbox" data-gallery="gallery">
			<img src="/images/gallery/36.jpg" alt="" />
		</a>
		<a href="/images/gallery/37.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/37.jpg" alt="" />
		</a>
		<a href="/images/gallery/38.jpg" class="wide glightbox" data-gallery="gallery">
			<img src="/images/gallery/38.jpg" alt="" />
		</a>
		<a href="/images/gallery/39.jpg" class="big glightbox" data-gallery="gallery">
			<img src="/images/gallery/39.jpg" alt="" />
		</a>
		<a href="/images/gallery/40.jpg" class="tall glightbox" data-gallery="gallery">
			<img src="/images/gallery/40.jpg" alt="" />
		</a>
		<a href="/images/gallery/41.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/41.jpg" alt="" />
		</a>
		<a href="/images/gallery/42.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/42.jpg" alt="" />
		</a>
		<a href="/images/gallery/43.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/43.jpg" alt="" />
		</a>
		<a href="/images/gallery/44.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/44.jpg" alt="" />
		</a>
		<a href="/images/gallery/45.jpg" class="wide glightbox" data-gallery="gallery">
			<img src="/images/gallery/45.jpg" alt="" />
		</a>
		<a href="/images/gallery/46.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/46.jpg" alt="" />
		</a>
		<a href="/images/gallery/47.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/47.jpg" alt="" />
		</a>
		<a href="/images/gallery/48.jpg" class="wide glightbox" data-gallery="gallery">
			<img src="/images/gallery/48.jpg" alt="" />
		</a>
		<a href="/images/gallery/49.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/49.jpg" alt="" />
		</a>
		<a href="/images/gallery/50.jpg" class="wide glightbox" data-gallery="gallery">
			<img src="/images/gallery/50.jpg" alt="" />
		</a>
		<a href="/images/gallery/51.jpg" class="big glightbox" data-gallery="gallery">
			<img src="/images/gallery/51.jpg" alt="" />
		</a>
		<a href="/images/gallery/52.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/52.jpg" alt="" />
		</a>
		<a href="/images/gallery/53.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/53.jpg" alt="" />
		</a>
		<a href="/images/gallery/54.jpg" class="big glightbox" data-gallery="gallery">
			<img src="/images/gallery/54.jpg" alt="" />
		</a>
		<a href="/images/gallery/55.jpg" class="tall glightbox" data-gallery="gallery">
			<img src="/images/gallery/55.jpg" alt="" />
		</a>
		<a href="/images/gallery/56.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/56.jpg" alt="" />
		</a>
		<a href="/images/gallery/57.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/57.jpg" alt="" />
		</a>
		<a href="/images/gallery/58.jpg" class="glightbox" data-gallery="gallery">
			<img src="/images/gallery/58.jpg" alt="" />
		</a>
	</div>
</div>
@endsection

@section('postscripts')
	<script type="text/javascript">
		var lightbox = GLightbox({
			touchNavigation: true,
		    loop: true,
		});
	</script>
@endsection