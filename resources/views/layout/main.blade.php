<!doctype html>
<html lang="en" xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <title>Geordie Crawl</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="initial-scale=1, width=device-width, height=device-height, viewport-fit=cover, user-scalable=no">
        <link href="/css/app.css?v=1.1.3" rel="stylesheet" type="text/css" />
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
             fbq('init', '1572772389791589'); 
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" src="https://www.facebook.com/tr?id=1572772389791589&ev=PageView&noscript=1"/>
        </noscript>
    </head>
    <body>
        <div id="app-vue">
            @if(isset($header))
                @include('components.header')
            @endif
            @yield('content')
            @if(Route::currentRouteName() != 'contact')
                @include('components.enquire-button')
            @endif
            @include('components.footer')

            <mobile-menu></mobile-menu>
        </div>

        @yield('prescripts')
        <script src="https://js.stripe.com/v3/"></script>
        <script src="https://kit.fontawesome.com/3b63e9377a.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="/js/app.js?v=1.1.3"></script>
        @yield('postscripts')
    </body>
</html>