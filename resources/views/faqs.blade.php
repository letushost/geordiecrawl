@extends('layout.main', ['header' => true])

@section('content')
<div class="page page-faqs">
	@include('components.book-banner')

	<section class="faqs">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="code">
						<h2>Is there a dress code?</h2>
						<p>Dress to impress, we are on a night out after all. Mainly casual/smart dress is fine.</p>
						<p>There is no fancy dress allowed on the bar crawl, but this can be arranged by booking a private crawl and advising us that you intend to be in fancy dress.</p>

						<h2>How do we get from bar to bar?</h2>
						<p>We Walk/Crawl from venue to venue and the Geordie party hosts will be there to guide you in a fun and safe manner.</p>

						<h2>What time does the crawl start?</h2>
						<p>The crawls start at 7pm but we ask you to arrive at the first venue, 15 minutes prior. This is to ensure you have a drink in your hand and are ready to start on time.</p>

						<h2>What time does the crawl end?</h2>
						<p>We aim to have you in the last venue (club) no later than 11.30pm as long as no one drags their feet.</p>

						<h2>What if we are running late?</h2>
						<p>We will give you a contact number to inform us If you’re running late. We will then arrange a meeting point for you to join the rest of the group. Please note that to get the full experience of the crawl, we ask you to join us from the first venue.</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="terms">
						<h2>Terms &amp; Conditions</h2>
						<p>Please be aware that ID may be requested at all venues</p>
						<p>The management reserve the right to refuse any offer.</p>
						<p>Know your own limits when drinking. You will be refused entry to any venue if you are overly drunk.  Geordie Crawl will not offer any refund if this happens.</p>
						<p>Please be aware that we operate a zero tolerance drug policy and we ask that you do not be offended if we request that you are searched upon entry or inside the venue. Anyone found with illegal drugs will be prohibited entry (as will your whole party) or removed from the venue and you may be reported to the police. Also, anyone found to be asking for drugs or offering drugs will be removed and banned for life. Again all prepaid money will be forfeited in this situation</p>
						<p>Respect is important at all times, to other Geordie Crawlers, door staff, venue staff and the general public, while on route to different venues.</p>
					</div>

					<p style="margin: 20px 0 0 0;">If there's anything that concerns you before planning your crawl, just <a href="#">drop us a message</a> and we'll help put your mind at ease</p>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection