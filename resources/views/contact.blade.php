@extends('layout.main', ['header' => true])

@section('content')
<div class="page page-contact">
	@include('components.book-banner')

	<section class="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<contact></contact>
				</div>
				<div class="col-md-6">
					<div class="help">
						<h2>Geordie Crawl</h2>
						<p>We can also arrange VIP tables in any of the nightclubs for after the crawl, aswell as dwarf hire, roly-poly strippers and much more.</p>
						<p>We have a great relationship with selected hotels if you are still looking for accomodation, please ask for prices.</p>
					</div>

					<div class="row contact-info">
						<div class="col-md-6">
							<span class="far fa-mobile"></span>
							<p>07946046928</p>
						</div>

						<div class="col-md-6 text-right">
							<span class="far fa-envelope"></span>
							<p>hello@geordiecrawl.com</p>
						</div>
					</div>

					<ul class="socials">
	                    <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
	                    <li><a href="#"><span class="fab fa-instagram-square"></span></a></li>
	                </ul>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('postscripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$('#date').flatpickr({
				minDate: 'today',
				disableMobile: true,
			});
		});
	</script>

	<style>
		.btn-enquire {display: none !important;}
	</style>
@endsection