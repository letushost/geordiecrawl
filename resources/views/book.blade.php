@extends('layout.main', ['header' => true])

@section('content')
<div class="page page-book">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="ticket">
					<img src="/images/ticket.jpg" class="img-responsive" />
					<h2>Geordie Crawl Ticket</h2>
					<span class="badge">&pound;25.00 Group (per person)</span>
					<span class="badge badge-dark">&pound;30.00 Private (per person)</span>

					<p>We can also arrange VIP tables in any of the nightclubs for after the crawl, aswell as dwarf hire, roly-poly strippers and much more.</p>
					<p>We have a great relationship with selected hotels if you are still looking for accomodation, please ask for prices.</p>
				</div>
			</div>
			<div class="col-md-6">
				<book></book>
			</div>
		</div>
	</div>
</div>
@endsection