@extends('layout.main')

@section('content')
<div class="page page-home">
    <div class="hero">
        <header>
            <div class="container">
                <div class="row row-condensed">
                    <div class="col-xs-12 col-md-2">
                        <img src="/images/logo-cutout-white-full.png" class="img-responsive" />
                    </div>
                    <div class="hidden-xs hidden-sm col-md-8">
                        <ul>
                            <li><a href="{{ route('home') }}" class="active">Home</a></li>
                            <li><a href="{{ route('gallery') }}">Gallery</a></li>
                            <li><a href="{{ route('faqs') }}">FAQs</a></li>
                            <li><a href="{{ route('contact') }}">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-2">
                        <a href="{{ route('book') }}" class="btn btn-primary">Book now</a>
                    </div>
                </div>
            </div>
        </header>

        <img src="/images/gohard-gohome.png" class="gohard-gohome" />
    </div>

    <div class="hero-mobile">
        <div class="container">
            <div class="top">
                <div class="row">
                    <div class="col-xs-6">
                        <img src="/images/logo-cutout-white-full.png" class="img-responsive" />
                    </div>
                </div>

                <a href="#" class="open-menu" @click.prevent="$root.$emit('toggleSidebar')"><span class="fa fa-bars"></span></a>
            </div>

            <img src="/images/gohard-gohome.png" class="gohard-gohome img-responsive" />
            <a href="{{ route('book') }}" class="btn btn-primary">Book Now</a>
        </div>
    </div>

    <div class="venues">
        <div class="item"><img src="/images/venues/bar52.jpg" /></div>
        <div class="item"><img src="/images/venues/retro.jpg" /></div>
        <div class="item"><img src="/images/venues/howlers.jpg" /></div>
        <div class="item"><img src="/images/venues/the-hustle.jpg" /></div>
        <div class="item"><img src="/images/venues/lofts.jpg" /></div>
        <div class="item"><img src="/images/venues/points.jpg" /></div>
        <div class="item"><img src="/images/venues/bijoux.jpg" /></div>
        <div class="item"><img src="/images/venues/flares.jpg" /></div>
        <div class="item"><img src="/images/venues/bier-keller.jpg" /></div>
        <div class="item"><img src="/images/venues/wonder-bar.jpg" /></div>
    </div>

    {{-- <img src="https://geordiecrawl.com/wp-content/uploads/2021/09/Geordie-Crawl-Logos-01-scaled.jpg" style="width: 100%;" /> --}}

    <section class="about">
        <div class="container">
            <div class="row row-lg">
                <div class="col-md-8 col-md-offset-2">
                    <h2>Prepare for a <span>belta</span> of a <em>pub crawl</em> in Newcastle</h2>
                    <p>Geordie crawl is Newcastle’s ultimate bar crawl!! We have worked in the cities nightlife industry for over 10 years and we know the best bars and clubs in the Toon. We will 100% show you the best night Newcastle has to offer and a <strong>night you won’t forget</strong>.</p>
                    <p>Our bar crawls are perfect for stags, hens, birthdays or for anyone just wanting an excuse for a night out. All our venues have been handpicked to make sure they are suitable for everyone on our crawls. Whether you’re 18 or 80, we will show you how us Geordies like to party.</p>
                    <p>The crawls will take you to 4/5 belta bars and end up in one of the most popular nightclubs as long as you can ‘Go hard & not go home'. Your Geordie Crawl party hosts (who have been chosen for their style, banter and all round good craic) will guide you through the Toon getting you the best drink deals, queue jumps and VIP areas, as well as playing party games that will have drink challenges, prizes and forfeits.</p>
                    <p>So look no further than Geordie Crawl, to arrange the best night out Newcastle has to offer</p>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ route('book') }}" class="btn btn-primary">Book me up</a>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ route('gallery') }}" class="btn btn-default">View the Gallery</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="features">
        <div class="container">
            <div class="row">
                <div class="row row-flex row-xl">
                    <div class="col-md-4 col-sm-6 col-1">
                        <div class="feature">
                            <div class="icon">
                                <span class="far fa-party-horn"></span>
                            </div>
                            <h2>Party <span>Hosts</span></h2>
                            <p>Newcastle locals who are trained to take you on the best night out, that the Toon has to offer</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-1">
                        <div class="feature">
                            <div class="icon">
                                <span class="far fa-champagne-glasses"></span>
                            </div>
                            <h2>Exclusive <span>Deals</span></h2>
                            <p>Each venue will have exclusive drink deals for Geordie Crawlers only</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-1">
                        <div class="feature">
                            <div class="icon">
                                <span class="far fa-piggy-bank"></span>
                            </div>
                            <h2>Free <span>Entry</span></h2>
                            <p>Entries to the nightclubs are already included in the Geordie crawl price</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-1">
                        <div class="feature">
                            <div class="icon">
                                <span class="far fa-dice"></span>
                            </div>
                            <h2>Party <span>Games</span></h2>
                            <p>Wild bar crawl games that include drinking challenges, forfeits & prizes</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-1">
                        <div class="feature">
                            <div class="icon">
                                <span class="far fa-crown"></span>
                            </div>
                            <h2>VIP <span>Areas</span></h2>
                            <p>Geordie Crawlers will have designated VIP areas in each bar. These are perfect for adding on any extras to your booking, i.e. strippers</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-1">
                        <div class="feature">
                            <div class="icon">
                                <span class="far fa-user-group-crown"></span>
                            </div>
                            <h2>No <span>Queues</span></h2>
                            <p>We get queue jumps in each venue so you don’t waste any valuable drinking time</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonials">
        <div class="container">
            <span class="far fa-chevron-left owl-left"></span>
            <span class="far fa-chevron-right right owl-right"></span>

            <div class="owl-carousel">
                <div class="item">
                    <div class="inner">
                        <h3>Read What</h3>
                        <h2>Amy Florence</h2>
                        <h4>Had to say</h4>
                        <p>“Had the best night with Grant, he really got all 13 of us in a fantastic mood from the very beginning.. he’s priceless and we want to permanently rent him! Thank you Grant for an awesome night, can’t wait to see the pictures!”</p>
                    </div>
                </div>

                <div class="item">
                    <div class="inner">
                        <h3>Read What</h3>
                        <h2>Amanda Shaw</h2>
                        <h4>Had to say</h4>
                        <p>“Fab evening for my daughters hen do. Fabulous not having to think” what pub are we off to next”!! All organised and professional. Great fun we all had! Would recommend it to everyone. Thanks again xx”</p>
                    </div>
                </div>

                <div class="item">
                    <div class="inner">
                        <h3>Read What</h3>
                        <h2 style="font-size: 3.6em;">Lorcan O Hanlon</h2>
                        <h4>Had to say</h4>
                        <p>“Had a brilliant time on this crawl with Aaron. He delivered all his promises for the night and all 12 of us had a class night! Big thanks to Aaron for being the brilliant host that he was. Would highly recommend for groups visiting Newcastle for a night out”</p>
                    </div>
                </div>

                <div class="item">
                    <div class="inner">
                        <h3>Read What</h3>
                        <h2>Raych Drury</h2>
                        <h4>Had to say</h4>
                        <p>”Never been on a bar crawl before and It exceeded my expectations and the bride to be and all the other hens loved it! Thanks for an amazing night ”</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('postscripts')
    <script type="text/javascript">
        $(document).ready(function() {
            var width = $(window).innerWidth(),
                height = $(window).innerHeight();

            $('.hero').css({
                width: width,
                height: height,
            });

            var carousel = $('.owl-carousel').owlCarousel({
                loop: true,
                items: 1,
                autoplay: true,
                autoplayTimeout: 5000,
                autoHeight: true,
            });

            $('.owl-left').on('click', function(e) {
                carousel.trigger('prev.owl.carousel');
            });

            $('.owl-right').on('click', function(e) {
                carousel.trigger('next.owl.carousel');
            });

            var venue_carousel = $('.venues').owlCarousel({
                loop: true,
                items: 3,
                autoplay: true,
                autoplayTimeout: 2000,
                responsive: {
                    500: {
                        items: 6,
                    }
                }
            }).addClass('owl-carousel');
        });
    </script>
@endsection