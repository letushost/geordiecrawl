<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {return view('home');})->name('home');
Route::get('/gallery', function() {return view('gallery');})->name('gallery');
Route::get('/faqs', function() {return view('faqs');})->name('faqs');
Route::get('/contact', function() {return view('contact');})->name('contact');
Route::get('/book', function() {return view('book');})->name('book');