<?php

Route::post('/checkout/intent', 'CartController@intent');
Route::post('/checkout/payment', 'CartController@payment');