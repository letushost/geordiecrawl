<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use App\Models\Order;
use Illuminate\Http\Request;
use Stripe\Stripe;

class CartController extends Controller {
	public function intent(Request $request)
	{
		\Stripe\Stripe::setApiKey('sk_live_wLqUnyREjyLh16tGXIowLbJ9');

		$ticket_price = ($request->ticket == 'basic') ? 25 : 30;
		$cart_price = $ticket_price * $request->quantity;

		// Coupon Code
		if($request->has('coupon') && $request->coupon != '') {
			$coupon = Coupon::where('code', $request->coupon)->firstOrFail();
			
			// Apply coupon discount
			$discount = ($coupon->discount / 100) * $cart_price;
			$cart_price -= number_format($discount, 2);
		}

		$booking_fee = ($cart_price * 0.014) + 0.2;
		$total_price = $cart_price + $booking_fee;

		$intent = \Stripe\PaymentIntent::create([
		  	'amount' => (number_format($total_price, 2) * 100),
		  	'currency' => 'gbp',
		  	'statement_descriptor' => "GeordieCrawl",
		]);

		return $intent;
	}

	public function payment(Request $request)
	{
		$stripe = new \Stripe\StripeClient('sk_live_wLqUnyREjyLh16tGXIowLbJ9');
		$intent = $stripe->paymentIntents->retrieve($request->payment_id);
		$charge = $intent->charges->data[0];

		$order = new Order;
		$order->ticket = $request->ticket;
		$order->quantity = $request->quantity;
		$order->name = $request->name;
		$order->email = $request->email;
		$order->phone = $request->phone;
		$order->occassion = $request->occassion;
		if($request->has('referral')) $order->referral = $request->referral;
		$order->date = $request->date;
		$order->price = ($charge->amount / 100);
		$order->stripe_id = $charge->id;

		// Coupon
		if($request->has('coupon') && $request->coupon != '') {
			$coupon = Coupon::where('code', strtolower($request->coupon))->first();
			if($coupon) {
				$order->coupon_id = $coupon->id;
			}
		}

		$order->save();

		return $order;
	}
}